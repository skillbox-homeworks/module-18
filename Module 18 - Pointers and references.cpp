#include <string>
#include <iostream>

class Player
{
public:
	// Constructor
	Player()
	{
		Name = "Unknown";
		Score = 0;
	}

	Player(std::string _Name, int _Score) : Name(_Name), Score(_Score)
	{}
	// End of constructor

private:

	std::string Name;
	int Score;

public:

	std::string GetName()
	{
		return Name;
	}

	int GetScore()
	{
		return Score;
	}
};


void DescendingBubleSort(Player* PlayersArray, int PlayersNumber)
{
	for (int i = 0; i < PlayersNumber - 1; i++)
	{
		for (int j = 0; j < PlayersNumber - i - 1; j++)
		{
			if (PlayersArray[j].GetScore() < PlayersArray[j + 1].GetScore())
			{
				Player temp = PlayersArray[j];
				PlayersArray[j] = PlayersArray[j + 1];
				PlayersArray[j + 1] = temp;
			}
		}
	}
}


void ShowLeaderboard(Player* PlayersArray, int PlayersNumber)
{
	std::string NameColumnHeader = "Name";
	std::string ScoreColumnHeader = "Score";

	int MaxNameLength = NameColumnHeader.length();
	int MaxScoreLength = ScoreColumnHeader.length();

	for (Player* CurrentPlayer = PlayersArray; CurrentPlayer < PlayersArray + PlayersNumber; CurrentPlayer++)
	{
		if (MaxNameLength < CurrentPlayer->GetName().length())
		{
			MaxNameLength = CurrentPlayer->GetName().length();
		}
		if (MaxScoreLength < std::to_string(CurrentPlayer->GetScore()).length())
		{
			MaxScoreLength = std::to_string(CurrentPlayer->GetScore()).length();
		}
	}

	int MaxLength = MaxNameLength + MaxScoreLength + 7;

	char line = '-';
	char space = ' ';

	std::cout << "\n" << std::string(MaxLength, line) << std::endl;

	std::string SpacesForName = std::string(MaxNameLength - NameColumnHeader.length(), space);
	std::string SpacesForScore = std::string(MaxScoreLength - ScoreColumnHeader.length(), space);

	std::cout << "| Name" << SpacesForName << " | Score" << SpacesForScore << " |" << std::endl;

	std::cout << std::string(MaxLength, line) << std::endl;

	for (Player* CurrentPlayer = PlayersArray; CurrentPlayer < PlayersArray + PlayersNumber; CurrentPlayer++)
	{
		std::string CurrentNameString = CurrentPlayer->GetName();
		int CurrentNameLength = CurrentNameString.length();

		std::string SpacesForName = std::string(MaxNameLength - CurrentNameLength, space);

		std::string CurrentScoreString = std::to_string(CurrentPlayer->GetScore());
		int CurrentScoreLength = CurrentScoreString.length();

		std::string SpacesForScore = std::string(MaxScoreLength - CurrentScoreLength, space);

		std::cout << "| " << CurrentNameString << SpacesForName << " | " << CurrentScoreString << SpacesForScore << " |" << std::endl;
	}

	std::cout << std::string(MaxLength, line) << std::endl;
}


int main()
{
	int PlayersNumber;

	std::cout << "How many players: ";
	std::cin >> PlayersNumber;

	Player* Players = new Player[PlayersNumber];

	for (int i = 0; i < PlayersNumber; i++)
	{
		std::string PlayerName;
		int PlayerScore;

		std::cout << "\nEnter the name of player_" << i << ": ";
		std::cin >> PlayerName;

		std::cout << "Enter the score of player_" << i << ": ";
		std::cin >> PlayerScore;

		Players[i] = Player(PlayerName, PlayerScore);
	}

	DescendingBubleSort(Players, PlayersNumber);

	ShowLeaderboard(Players, PlayersNumber);

	delete[] Players;

	return 0;
}